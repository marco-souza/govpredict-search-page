# govpredict-search-page

Search page to GovPredict application test.

## TODO

- [ ] Add **Tests**
  - [ ] Jest
  - [ ] RxJS tests
- [ ] Pages
  - [ ] Search page
    - [ ] SearchComponent
      - [ ] search **text** field
      - [ ] search **group_list** field
      - [ ] search datarange [optional]
      - [ ] search social_media [optional]
    - [ ] ResultsComponent
      - [ ] results list
        - [ ] item: Message
        - [ ] item: User identity
        - [ ] item: Social Network
        - [ ] item: Range
        - [ ] item: Datetime
        - [ ] pagination: page list
- [ ] Change logo
- [ ] Change name
- [ ] Change desc

## Personalize

- change favicon
- PWA
